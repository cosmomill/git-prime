#!/bin/bash
###########################
#
# Usage:  See 
#
# How it works:
# This git-prime.sh script is meant to be run on a cron job.  Its goal is to provide an auto-save feature for a git repository.
# 
# Basically the script invokes: 
# $ git stash create
# Then it writes out the hash to the gitPrime.log with some more data.
# But it gets no hash back then you're on a clean branch and it grabs the HEAD hash and writes that out:
# $ git rev-parse HEAD
# 
# In both cases before it writes to the log it checks to see if that hash (or an equivalent hash obj with no differences) was just written. If so then it does not write anything to the log - nothing changed.
#
################
#
# notes: 
#
##################
branchName=`git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'`
gitPrimeFile=".git/git-prime.log";
gitPrimeStashHash=$(git stash create);
gitPrimeHEADHash=$(git rev-parse HEAD);
gitPrimeLastStash=".git/git-prime-last.log";
gitPrimeLastDiff=".git/git-prime-last.patch";
gitPrimeDiffLogDir=".git/git-prime-diffs";
gitPrimeRecipient="john.doe@example.com";

echo ""
echo "===== START git-prime script ===="
echo `date`;

# -- Here we simply make sure the two files gitPrime uses in a repository exist so the script doesn't give "file not found" errors.
touch $gitPrimeLastStash
touch $gitPrimeFile
touch $gitPrimeLastDiff
mkdir -p $gitPrimeDiffLogDir

# -- This is the mesaage we will record to the gitPrime.log file *if* something changed.
message=$(date; echo " branch: $branchName -- stash object created by git-prime (DIRTY Working Directory)");

git add . > /dev/null
length=${#gitPrimeStashHash}
lengthNum=$((length))

# -- check whether the command 'git stash create' handed back a hash.  If yes we might write it if git diff is 'true' (so to speak), but
# -- if 'git stash create' is empty then we are on a clean working dir. and skip all this.
if [ $lengthNum -gt 10 ]; then
	echo "dirty working dir - might write stash"

	lastStashHash=`cat $gitPrimeLastStash`
	foo=`git diff $lastStashHash $gitPrimeStashHash`
	length=${#foo}
	lengthNum=$((length))
	# -- Here we are comparing the git diff of the current 'stash hash' with the last one that we wrote.
	# -- if they are different then something changed and we record the new one to the gitPrime.log otherwise
	# -- nothing changed and we do nothing. 
	if [ $lengthNum -gt 5 ]; then
		echo "writing stash"
		echo $gitPrimeStashHash $message >> $gitPrimeFile;
		git diff $lastStashHash $gitPrimeStashHash > $gitPrimeLastDiff
		git diff $lastStashHash $gitPrimeStashHash > $gitPrimeDiffLogDir/$gitPrimeStashHash.patch
		git diff --color-words $lastStashHash $gitPrimeStashHash | ansi2html.sh > $gitPrimeDiffLogDir/$gitPrimeStashHash.html
		echo $gitPrimeStashHash $message | mailx -s "stash object created by git-prime" -a $gitPrimeLastDiff $gitPrimeRecipient
	else
		echo "last stash obj has no differences with current. not writing stash hash"
	fi
	echo $gitPrimeStashHash > $gitPrimeLastStash
	if [ -s !$gitPrimeFile ]; then
		echo $gitPrimeStashHash $message >> $gitPrimeFile;
	fi
else
	# -- we know that 'git stash create' handed nothing back so we are on a clean working dir.  So we skipped the stash stuff.
	# -- We check to see if the current HEAD has has already been captured in the gitPrime.log.  If it has not then we record it
	# -- otherwise we do not re-record it. Once is enough.
	echo "no dirty working dir. so might write HEAD hash"
	foo=`grep $gitPrimeHEADHash $gitPrimeFile`
	# -- Here we check to see if the HEAD hash already exists in the gitPrime.log.  If it does not already exist we log it otherwise we do nothing.
	if [ -z "$foo" ]; then
		echo "writing HEAD hash"
		message=$(date; echo "  branch: $branchName   -- HEAD hash captured by git-prime (CLEAN Working Directory)") ;
		echo $gitPrimeHEADHash $message >> $gitPrimeFile;
	else
		echo "NOT writing HEAD hash because it was found in the gitPrime log file"
	fi

fi

echo "----- END git-prime script -----"
